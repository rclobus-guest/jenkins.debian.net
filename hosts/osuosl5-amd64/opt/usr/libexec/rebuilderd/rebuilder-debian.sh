#!/bin/sh
set -xe
cd "$(dirname "$1")"

mkdir -p etc/apt
mkdir -p var/lib/apt/lists/
export TMPDIR=/srv/rebuilderd/tmp ; mkdir -p $TMPDIR ; chmod 777 $TMPDIR
echo 'deb-src [signed-by=/usr/share/keyrings/debian-archive-keyring.gpg] https://deb.debian.org/debian trixie main' > etc/apt/sources.list
apt-get -o Dir=. update
apt-get -o Dir=. source --download-only "$(basename "$1" | cut -d_ -f1)"

nice /usr/bin/debrebuild --buildresult="${REBUILDERD_OUTDIR}" --builder=sbuild+unshare --cache=/srv/rebuilderd/cache -- "${1}"
