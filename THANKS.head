== Thanks

Many many thanks to our sponsors and contributors! Without you, jenkins.debian.net would not exist today!

=== Sponsors

link:https://jenkins.debian.net/["jenkins.debian.net"] would not be possible without our sponsors:

 * Since October *2012* jenkins.debian.net has been running on (virtualized) hardware sponsored by link:https://www.ionos.com[IONOS] (formerly known as Profitbricks) - *&lt;blink&gt;Thank you very very very much!&lt;/blink&gt;*!
 * Currently the vast majority of our 'amd64' and 'i386' nodes are hosted there:
 ** 32 cores and 160 GB memory for jenkins.debian.net
 ** 20 cores and 80 GB memory for ionos1-amd64.debian.net used for building amd64 Debian packages for tests.reproducible-builds.org (t.r-b.o)
 ** 13 cores and 8 GB memory for ionos2-i386.debian.net used for building i386 Debian packages for t.r-b.o
 ** 21 cores and 80 GB memory for ionos5-amd64.debian.net used for building amd64 Debian packages for t.r-b.o, running in the future
 ** 12 cores and 8 GB memory for ionos6-i386.debian.net used for building i386 Debian packages for t.r-b.o, running in the future
 ** 20 cores and 80 GB memory for ionos11-amd64.debian.net used for building amd64 Debian packages for tests.reproducible-builds.org (t.r-b.o)
 ** 13 cores and 8 GB memory for ionos12-i386.debian.net used for building i386 Debian packages for t.r-b.o
 ** 21 cores and 80 GB memory for ionos15-amd64.debian.net used for building amd64 Debian packages for t.r-b.o, running in the future
 ** 12 cores and 8 GB memory for ionos16-i386.debian.net sed for building i386 Debian packages for t.r-b.o, running in the future
 ** 2 cores and 8 GB memory for ionos7-amd64.debian.net used for buildinfos.debian.net
 ** 5 cores and 10 GB memory for ionos9-amd64.debian.net used for rebootstrap jobs
 ** 4 cores and 12 GB memory for ionos10-amd64.debian.net used for chroot-installation jobs
 ** 9 cores and 19 GB memory for freebsd-jenkins.debian.net (also running on IONOS virtual hardware), used for building FreeBSD for t.r-b.o
 ** 2 cores and 12 GB memory for ionos4-amd64.debian.net used as a http(s)-proxy for one half of our ionos nodes
 ** 2 cores and 12 GB memory for ionos14-amd64.debian.net used as a http(s)-proxy for the other half of our ionos nodes
 * link:https://www.infomaniak.com/en/hosting/public-cloud[infomaniak's public cloud] also provides us with two 'amd64' nodes:
 ** 12 cores and 24 GB memory for infom01-amd64.debian.net used for building amd64 Debian packages for tests.reproducible-builds.org (t.r-b.o)
 ** 12 cores and 24 GB memory for infom02-amd64.debian.net used for building amd64 Debian packages for tests.reproducible-builds.org (t.r-b.o), running in the future
 ** 8 cores and 16 GB memory for infom01-i386.debian.net used for building i386 Debian packages for tests.reproducible-builds.org (t.r-b.o)
 ** 8 cores and 16 GB memory for infom02-i386.debian.net used for building i386 Debian packages for tests.reproducible-builds.org (t.r-b.o), running in the future
 * link:https://qa.debian.org/developer.php?login=vagrant%40debian.org[Vagrant] provides and hosts 13 'armhf' systems, used for building armhf Debian packages for t.r-b.o:
 ** four quad-cores with 4 GB RAM each,
 ** 'armhf' servers provided by linaro, hosted at Varant:
 *** one octo-core with 32GB of ram (divided into two virtual machines running at ~15GB ram each)
 *** two octo-core with 16GB of ram (divided into four virtual machines running at ~7GB ram each)
 * link:https://codethink.co.uk[Codethink] has supported us with 'arm64' nodes since December 2016, since October 2023 they are kindly providing these 4 kvm 'arm64' nodes for us:
 ** 12 cores and 64 GB memory for codethink01-arm64.debian.net used for building arm64 Debian packages for t.r-b.o, running in the future
 ** 12 cores and 64 GB memory for codethink02-arm64.debian.net used for building arm64 Debian packages for t.r-b.o
 ** 12 cores and 64 GB memory for codethink03-arm64.debian.net used for building arm64 Debian packages for t.r-b.o, running in the future
 ** 12 cores and 64 GB memory for codethink04-arm64.debian.net used for building arm64 Debian packages for t.r-b.o
 * link:https://letsencrypt.org[Let's encrypt] provides free of charge SSL certificates for jenkins.debian.net, reproducible.debian.net and tests.reproducible-builds.org.
 * In December 2018 we were given access to eight nodes which were donated by Facebook to the GCC Compile Farm project and are now hosted by link:https://osuosl.org/[OSUOSL] which each had 32 cores with 144 GB memory. Those machines have been retired now and OSUOSL offered different machines to us:
 * In spring 2023 we got access to some new nodes hosted by link:https://osuosl.org/[OSUOSL]:
  ** 16 cores with 128 GB memory for osuosl1-amd64.reproducible.osuosl.org used for building OpenWrt, coreboot and NetBSD for t.r-b.o
  ** 16 cores with 128 GB memory for osuosl2-amd64.reproducible.osuosl.org used for building OpenWrt, coreboot for t.r-b.o
  ** 16 cores with 128 GB memory for osuosl3-amd64.reproducible.osuosl.org used for building Debian live, Debian bootstrapping jobs, Debian janitor jobs, mmdebstrap-jenkins jobs and openqa.d.n workers
  ** 24 cores with 64 GB memory for osuosl4-amd64.reproducible.osuosl.org plus 16TB RAID on HDDs for developing https://rebuilder-snapshot.debian.net and other snapshot and rebuilding related experiments
  ** 16 cores with 64 GB memory for osuosl5-amd64.reproducible.osuosl.org plus 20TB RAID on SDDs for running and developing https://rebuilder-snapshot.debian.net

==== Past sponsors

 * In December 2016 link:https://codethink.co.uk[Codethink] kindly offered access to 8 arm64 build nodes on link:http://www8.hp.com/us/en/hp-news/press-release.html?id=1800094#.VgQdelox1QI[HP moonshot hardware] which got replaced with more modern kvm nodes by Codethink in October 2023.
 * link:https://globalsign.com[GlobalSign] from January 2015 to January 2016 provided free of charge SSL certificates for both jenkins.debian.net and reproducible.debian.net.

=== Contributors

link:https://jenkins.debian.net/["jenkins.debian.net"] would not be possible without these contributors:


----

