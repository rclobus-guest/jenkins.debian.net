#!/bin/bash

# Copyright 2015-2024 Holger Levsen <holger@layer-acht.org>
# released under the GPLv2

# define Debian build nodes in use for tests.reproducible-builds.org/debian/
# 	FIXME: this is used differently in two places,
#		- bin/reproducible_html_nodes_info.sh
#		  where it *must* only contain the Debian nodes as it's used
#		  to generate the variations… and
#		- bin/reproducible_cleanup_nodes.sh where it would be
#		  nice to also include ionos4,7,9,10+14, to also cleanup
#		  jobs there…
#	FIXME: this list is also defined in two places:
#		- as the list below can also be constructed like this: ./nodes/list_nodes |grep -Ev "(rb-mail|jenkins|ionos4|ionos7|ionos9|ionos10|ionos14|osuosl4|osuosl5)"
BUILD_NODES="
cbxi4a-armhf-rb.debian.net
cbxi4b-armhf-rb.debian.net
cbxi4pro0-armhf-rb.debian.net
codethink03-arm64.debian.net
codethink04-arm64.debian.net
ff4a-armhf-rb.debian.net
ff64a-armhf-rb.debian.net
ionos1-amd64.debian.net
ionos2-i386.debian.net
ionos5-amd64.debian.net
ionos6-i386.debian.net
ionos11-amd64.debian.net
ionos12-i386.debian.net
ionos15-amd64.debian.net
ionos16-i386.debian.net
virt32a-armhf-rb.debian.net
virt32b-armhf-rb.debian.net
virt32c-armhf-rb.debian.net
virt32z-armhf-rb.debian.net
virt64a-armhf-rb.debian.net
virt64b-armhf-rb.debian.net
virt64c-armhf-rb.debian.net
virt64z-armhf-rb.debian.net
wbq0-armhf-rb.debian.net
osuosl1-amd64.debian.net
osuosl2-amd64.debian.net
osuosl3-amd64.debian.net
infom01-amd64.debian.net
infom02-amd64.debian.net"

NODE_RUN_IN_THE_FUTURE=false
get_node_information() {
	local NODE_NAME=$1
	case "$NODE_NAME" in
	  ionos[56]*|ionos1[56]*)
	    NODE_RUN_IN_THE_FUTURE=true
	    ;;
	  codethink03*)
	    NODE_RUN_IN_THE_FUTURE=true
	    ;;
	  osuosl2*)
	    NODE_RUN_IN_THE_FUTURE=true
	    ;;
	  infom02*)
	    NODE_RUN_IN_THE_FUTURE=true
	    ;;
	  *)
	    ;;
	esac
}
