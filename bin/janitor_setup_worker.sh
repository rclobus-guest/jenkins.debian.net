#!/bin/bash

# Set up the build environment for a janitor worker.

set -e

DEBUG=true
. /srv/jenkins/bin/common-functions.sh
common_init "$@"

docker pull eu.gcr.io/debian-janitor/worker

# vim: set sw=0 noet :
