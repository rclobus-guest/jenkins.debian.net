#!/bin/bash
# vim: set noexpandtab:

# Copyright © 2017-2024 Holger Levsen (holger@layer-acht.org)
# Copyright © 2018-2024 Mattia Rizzolo <mattia@debian.org>
# released under the GPLv2

#
# services to run many workers running reproducible_build.sh
#
# to see running workers:
# sudo systemctl --user -M jenkins@ list-units -t service

set -e

startup_workers() {
	local SERVICE_NAME
	local WORKER_BIN WORKER_NAME
	local BUILD_BASE

	WORKER_BIN=/srv/jenkins/bin/reproducible_worker.sh

	awk '!/^\s*#/ {print $1}' /srv/jenkins/bin/reproducible_build_workers | \
		while read -r WORKER_NAME ; do
			if [ -z "$WORKER_NAME" ]; then
				# empty line…
				continue
			fi
			# sleep up to 2.3 seconds (additionally to the random sleep reproducible_build.sh does anyway)
			#
			/bin/sleep "$(echo "scale=1 ; $(shuf -i 1-23 -n 1)/10" | bc )"

			SERVICE_NAME="reproducible_build@${WORKER_NAME}.service"

			# first check the status of the services
			# TODO: commented for now, let's first see how it goes without this,
			# as it probably needs quite some tweaking
			#local serviceSubState serviceActiveState
			#serviceSubState=$(systemd show -P SubState "$SERVICE_NAME")
			#serviceActiveState=$(systemd show -P ActiveState "$SERVICE_NAME")
			#if [ "$serviceActiveState" = "active" ] && [ "$serviceSubState" = "exited" ]; then
			#	# there is probably a good reason for this, don't restart it
			#	echo "$(date --utc) - $WORKER_NAME already exited, not restarting.  Check log."
			#	systemctl status "$SERVICE_NAME"
			#	continue
			#fi

			# systemd can be in weird state, so just look at what real
			# processes are around, don't look at what systemd think it's running
			if pgrep -f "$WORKER_BIN $WORKER_NAME\$" > /dev/null ; then
				echo "$(date --utc) - '$(basename $WORKER_BIN) $WORKER_NAME' already running, thus moving on to the next."
				continue
			fi

			#
			# actually start the worker
			#
			if [ -z "${XDG_RUNTIME_DIR:-}" ]; then
				XDG_RUNTIME_DIR="/run/user/$UID"
				export XDG_RUNTIME_DIR
			fi
			BUILD_BASE=/var/lib/jenkins/userContent/reproducible/debian/build_service/$WORKER_NAME
			mkdir -p "$BUILD_BASE"
			echo "$(date --utc) - Starting $WORKER_NAME"
			systemctl --user start "$SERVICE_NAME"
	done
}


check_lock() {
	local LOCKFILE children
	LOCKFILE="/var/lib/jenkins/NO-RB-BUILDERS-PLEASE"
	if [ -f "$LOCKFILE" ]; then
		echo "The lockfile $LOCKFILE is present, exiting the service..."
		while : ; do
			children="$(pgrep --list-full --parent $$ || true)"
			if [ -n "$children" ]; then
				local SLEEPTIME=15m
				echo "There are still some child processes, waiting $SLEEPTIME for them:"
				echo "$children"
				sleep $SLEEPTIME
			else
				exit 9
			fi
		done
	fi
}

#
# main, keep running forever…
#
while true ; do
	check_lock
	#
	# this is all we do
	#
	startup_workers
	#
	# now sleep, but allow wakeup calls
	#
	set +e
	sleep 133.7m
	set -e
done
