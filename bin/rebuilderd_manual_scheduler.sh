#!/bin/bash
#
# Copyright 2025 Holger Levsen (holger@layer-acht.org)
# released under the GPLv2

set -e

#
# choose correct port
#
case $1 in
	all)		PORT=8489
			;;
	amd64)		PORT=8484
			;;
	arm64)		PORT=8486
			;;
	armhf)		PORT=8487
			;;
	i386)		PORT=8485
			;;
	riscv64)	PORT=8488
			;;
	*)		echo "need a valid architecture."
			exit 1
			;;
esac

ARCH=$1
shift
PKGS=$@
SUCCESSES=""
s=0
FAILURES=""
f=0
for i in $PKGS ; do
	i=$(echo $i | tr -d 🍥 | tr -d ♻ | tr -d 💠)
	echo -n "Scheduling $i: "
	if env REBUILDERD_COOKIE_PATH=~/.local/share/rebuilderd-auth-cookie /opt/usr/bin/rebuildctl -H http://127.0.0.1:$PORT pkgs requeue --reset --distro debian --name $i ; then
		SUCCESSES="$SUCCESSES $i"
		let s+=1
		echo ok
	else
		FAILURES="$FAILURES $i"
		let f+=1
	fi
done
echo
if [ -z "$FAILURES" ] ; then
	echo
	echo All $s requested packages successfully scheduled on $ARCH.
else
	echo Failed to schedule $f packages on $ARCH: $FAILURES
	echo Successfully scheduled $s packages on $ARCH: $SUCCESSES
	echo
	echo Successfully scheduled $s packages on $ARCH, failed to schedule $f packages.
fi
