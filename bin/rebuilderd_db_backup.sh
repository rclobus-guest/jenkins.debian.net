#!/bin/bash
#
# Copyright 2024-2025 Holger Levsen (holger@layer-acht.org)
# released under the GPLv2

set -e

#
# WIP. currently to be run manually as the db user...
# should be called by a systemd timer on boot before rebuilderd
# is started. (and then rebuilderd can be started once its compressing in background...)
# currently its ment to be run manually.
# rbuilderd should not be running when this is run to ensure db consistancy.
#
if [ -z "$1" ] ; then
	echo "need an architecture."
	exit 1
fi

ARCH=$1
BASE_PATH=/srv/rebuilderd/$ARCH
TODAY=$(date '+%Y-%m-%d' -u)
BACKUP_DIR=$BASE_PATH/stats/db-backup
mkdir -p $BACKUP_DIR
cd $BACKUP_DIR

mkdir db-back-${TODAY}
cp -av $BASE_PATH/rebuilderd.db* db-back-${TODAY}/ 
echo 
echo "it's safe to restart rebuilderd again."
echo
cd db-back-${TODAY}
for i in rebuilderd.db* ; do 
	xz --verbose $i
done
echo
du -sch $BASE_PATH/rebuilderd.db*
echo
du -sch $BACKUP_DIR/*

