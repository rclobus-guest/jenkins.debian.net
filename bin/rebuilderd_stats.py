#!/usr/bin/python3

from datetime import datetime
from os import path
from re import DOTALL, search, sub
from sqlite3 import connect
from sys import argv

from psutil._common import bytes2human


def main() -> None:
    arch = argv[1]
    db = argv[2]
    cx = connect(db)
    cx.create_function("regexp", 2, lambda x, y: 1 if search(x, y) else 0)
    cu = cx.cursor()

    def log_has(s):
        return lambda log, _: s in log

    def log_has_re(re):
        return lambda log, _: search(re, log, DOTALL)

    def log_has_any(strs):
        def f(log, _):
            return any(s in log for s in strs)
        return f

    def log_has_all(strs):
        def f(log, _):
            return all(s in log for s in strs)
        return f

    def diff_has(s):
        return lambda _, diff: s in (diff or '')

    def diff_has_re(re):
        return lambda _, diff: search(re, diff or '', DOTALL)

    def diff_has_any(strs):
        def f(_, diff):
            if not diff:
                return False

            return any(s in (diff or '') for s in strs)
        return f

    error_messages = {
        "rebuilderd failed, no diskspace (temporary)": log_has_re(r"(^rebuilderd: unexpected error while rebuilding package: Failed to download original package from.*: No space left on device \(os error 28\)|^E: Failed to fetch https://deb.debian.org/debian/dists/.*/main/source/Sources)"),
        "buildinfo file 404 (maybe temporary)": log_has("rebuilderd: unexpected error while rebuilding package: Failed to download build input from"),
        "package file 404 (temporary)": log_has("rebuilderd: unexpected error while rebuilding package: Failed to download original package from"),
        "packages missing on metasnap (maybe temporary)": log_has_re(r"cannot find:.*debootsnap failed at /usr/bin/debrebuild line 48"),
        "multiarch-support missing (NMUs needed)": log_has_all(["multiarch-support but it is not installable", "Build Type: all"]),
        "multiarch-support missing (binNMUs needed)": log_has("multiarch-support but it is not installable"),
        "mmdebstrap failed (#1094165)": log_has_all(["fatal: `/bin/chfn -f Debian OWFS system account Debian-ow' returned error code 1. Exiting.", "debootsnap failed at /usr/bin/debrebuild line 48"]),
        "debootsnap failed (temporary)": log_has_all(["Failed to fetch http://snapshot.debian.org/archive/debian", "debootsnap failed at /usr/bin/debrebuild line 48"]),
        "debootsnap failed (maybe temporary)": log_has("debootsnap failed at /usr/bin/debrebuild line 48"),
        "debootsnap failed, no diskspace (temporary)": log_has_any(["W: creating tarball failed: E: cannot copy to /tmp/debrebuild", "dpkg-new': failed to write (No space left on device)"]),
        "dpkg-source failed (maybe temporary)": log_has("E: FAILED [dpkg-source died]"),
        "network error (temporary)": log_has("Unable to connect to 127.0.0.1:3128"),
        "download failed (temporary)": log_has_re("400 URL must be absolute.E: Could not download.*sbuild failed"),
        "sbuild chroot failed (temporary)": log_has("E: Error creating chroot session: skipping"),
        "sbuild failed due to insufficient disk space": log_has("E: Disk space is probably not sufficient for building."),
        "timeout: freedict (#998683)": log_has_all(["TRUNCATED DUE TO TIMEOUT: ", "inputs/freedict_20"]),
        "timeout (maybe temporary)": log_has("TRUNCATED DUE TO TIMEOUT: "),
        "old dpkg (<1.19.0, (bin)NMUs needed)": log_has_re(r"dpkg is already the newest version \(1\.1[0-8].*fakeroot not found, either install the fakeroot"),
        "fakeroot not found (https://deb.li/3P46G)": log_has("fakeroot not found, either install the fakeroot"),
        "failed to reproduce: dh-r (#1089197)": log_has_re(r"(Source|Binary): r-(cran|bioc|other)-"),
        "dpkg-buildpackage failed": log_has("E: Build failure (dpkg-buildpackage died"),
        "diffoscope timeout (not fatal)": diff_has("TRUNCATED DUE TO TIMEOUT: 600 seconds"),
        "failed to reproduce: dh-buildinfo (#1068809)": diff_has_any([f"buildinfo_{arch}.gz", "buildinfo_all.gz"]),
        "failed to reproduce: 1-second offset (#1089088)": diff_has_re(r" -([drwx-]{10} [0-9a-z() ]{,60} [0-9]+ [0-9-]{10} [012][0-9]:).* \+\1"),
        "failed to reproduce": lambda log, diff: diff,
        "size limit (not fatal)": log_has("TRUNCATED DUE TO SIZE LIMIT: "),
        "other errors": lambda *_: True,
    }

    messages_packages = {k: [] for k in error_messages.keys()}  # Preserve keys order
    total = 0
    for row in cu.execute(
        "SELECT p.name, s.name, p.build_id, p.has_diffoscope, CAST(b.build_log AS TEXT), SUBSTR(b.diffoscope, 1, 100000)"
        " FROM packages p JOIN pkgbases s ON s.id = p.pkgbase_id LEFT JOIN builds b ON b.id = p.build_id"
        " WHERE p.status ='BAD' ORDER BY p.build_id"
    ):
        total += 1
        for message, error_match_fn in error_messages.items():
            if error_match_fn(row[4], row[5]):
                messages_packages[message].append({"name": row[0], "src": row[1], "id": row[2], "diff": bool(row[3])})
                break
    messages_packages = {k: v for k, v in messages_packages.items() if v}  # Remove empty categories

    print(
        '<!DOCTYPE html><html lang="en"><head>'
        '<meta charset="utf-8">'
        f"<title>https://reproduce.debian.net/{arch}/stats</title>"
        '<meta name="viewport" content="width=device-width, initial-scale=1">'
        "</head><body>"
        f'<header><h1>https://<a href="/">reproduce</a>.debian.net/<a href="/{arch}">{arch}</a>/stats</h1></header> <main>'
    )
    print(f"Database size: {bytes2human(path.getsize(db))}<br/>")
    print(f"Last changed: {datetime.now().replace(microsecond=0)} - updated every 3h.")

    print("<table> <tr> <th>error</th> <th colspan=2>number of affected bad packages</th> </tr>")
    for message, packages in messages_packages.items():
        anchor = message.replace(" ", "-")
        print(
            f'<tr><td><a href="#{anchor}">{message}</a></td>'
            f'<td style="text-align:right">{len(packages)}</td><td style="text-align:right">({len(packages)/total*100:.2f}%)</td></tr>'
        )
    print("</table>")

    for message, packages in messages_packages.items():
        anchor = message.replace(" ", "-")
        message = sub(r'(#)([0-9]*)', r'https://bugs.debian.org/\2', message)
        message = sub(r'(https://[a-z.A-Z_0-9/]*)', r'<a href="\1">\1</a>', message)
        print(f'\n<h2 id="{anchor}">{message}</h2>')
        span = 0
        print("<p><span>")
        for pkg in packages:
            print(f'<a href="https://reproduce.debian.net/{arch}/api/v0/builds/{pkg["id"]}/log">{pkg["name"]}</a>', end='')
            if pkg["diff"]:
                print(f'<a href="https://reproduce.debian.net/{arch}/api/v0/builds/{pkg["id"]}/diffoscope">💠</a>', end='')
            print(f'<a href="https://tracker.debian.org/pkg/{pkg["src"]}">🍥</a>', end='')
            print(f'<a href="https://tests.reproducible-builds.org/debian/rb-pkg/trixie/{arch}/{pkg["src"]}.html">♻</a>', end='')
            print(" ")
            span += 1
            if span == 256:
                span = 0
                print("</span><span>")
        print("</span> ")
        print("</p>")

    print('<br/><br/><hr/><a href="https://salsa.debian.org/qa/jenkins.debian.net/-/blob/master/bin/rebuilderd_stats.py">rebuilderd_stats.py<a/> - patches welcome.</main></body></html>')


if __name__ == "__main__":
    main()
